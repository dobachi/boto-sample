#!/bin/env python

import boto.ec2
import json

conn = boto.ec2.connect_to_region("ap-northeast-1")
reservations = conn.get_all_reservations()
for reservation in reservations:
  m = {}
  for inst in reservation.instances:
    m = { "public_ip_address" : inst.ip_address,
          "private_ip_address" : inst.private_ip_address,
          "tags" : inst.tags}
    print json.dumps(m)

# vim: et ts=2 sw=2
