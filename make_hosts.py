#!/bin/env python

import boto.ec2
import json

def print_head():
  print "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4"
  print "::1         localhost localhost.localdomain localhost6 localhost6.localdomain6"
  print ""

def main():
  conn = boto.ec2.connect_to_region("ap-northeast-1")
  reservations = conn.get_all_reservations()
  
  master_num = 0
  slave_num = 0

  print_head()
  for reservation in reservations:
    m = {}
    for inst in reservation.instances:
      tags = inst.tags
      if tags["name"] == "master_slave_cluster" and tags["nodetype"] == "master":
        print inst.private_ip_address + " " + "master" + str(master_num)
      elif tags["name"] == "master_slave_cluster" and tags["nodetype"] == "slave":
        print  inst.private_ip_address + " " +"slave" + str(slave_num)
        slave_num = slave_num + 1

main()

# vim: et ts=2 sw=2
